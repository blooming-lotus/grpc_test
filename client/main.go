package main

import (
	"context"
	"log"

	api "gitlab.com/star191/grpc_test/api/v3"
	"google.golang.org/grpc"
)

func main() {
	var conn *grpc.ClientConn

	conn, err := grpc.Dial(":1337", grpc.WithInsecure())
	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	client := api.NewPingClient(conn)

	responce, err := client.SayHello(context.Background(), &api.PingMessage{Greeting: "foo"})
	if err != nil {
		log.Println(err)
	} else {
		log.Printf("Receive message: %s", responce.Greeting)
	}

}
