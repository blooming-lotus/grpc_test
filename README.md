# grpc_test

gRPC golang test

## Generate the gRPC .go file from .proto
`protoc -I api/proto/v3/ --go_out=plugins=grpc:api/v3 api/proto/v3/api.proto`

## Run
`go run ./server/main.go`

`go run ./client/main.go`