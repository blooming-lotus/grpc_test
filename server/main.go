package main

import (
	"context"
	"log"
	"net"

	api "gitlab.com/star191/grpc_test/api/v3"
	"google.golang.org/grpc"
)

type PingServer struct {
}

func (s *PingServer) SayHello(ctx context.Context, in *api.PingMessage) (*api.PingMessage, error) {
	log.Printf("Receive message %s", in.Greeting)
	return &api.PingMessage{Greeting: "bar"}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":1337")
	if err != nil {
		log.Println(err)
	}

	s := PingServer{}

	grpcServer := grpc.NewServer()

	api.RegisterPingServer(grpcServer, &s)

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalln(err)
	}

}
